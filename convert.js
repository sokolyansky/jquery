$('#selectSum').on('change', changeCurrency);
$('#selectVal').on('change', changeCurrency);

function setCurrency(currency, node) {
  if(node.innerHTML.slice(0, 3) == currency) {
    $(node).prop('selected', true);
  }
}
function changeCurrency() {
  var sum = +$('#selectSum').val();
  var $result = $('#result');
  var $selectVal = $('#selectVal option:selected');
  var $selectValTo = $('#selectValTo option').filter(':selected');

  var a = sum * +$selectVal.data('rate') / +$selectVal.data('nominal') /
    + $selectValTo.data('rate') * +$selectValTo.data('nominal');

  $result.val(Math.round(a * 100) / 100);
}


$.get('http://university.netology.ru/api/currency', function (data) {
  var res = $(data).map(function (i, item) {
    return '<option value = ' + item.CharCode + ' data-nominal = ' + item.Nominal +
      ' data-rate = ' + item.Value + ' >' + item.CharCode + ' - ' + item.Name + '</option>';
  });
  Array.prototype.unshift.call(res,
    '<option value = RUB data-nominal = 1 data-rate = 1>RUB - Российский рубль</option>');
  res = Array.prototype.join.call(res, '');

  $('#selectVal, #selectValTo').html(res);

  var $selectVal = $('#selectVal option');
  var $selectValTo = $('#selectValTo option');
  $selectVal.each(function (i, node) {
    setCurrency('RUB', node);
  });
  $selectValTo.each(function (i, node) {
    setCurrency('USD', node);
  });
  changeCurrency();
});
